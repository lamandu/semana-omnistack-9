const multer = require('multer');
const path = require('path');

module.exports = {
    storage: multer.diskStorage({
        destination: path.resolve(__dirname,'..','..', 'uploads'), // '..' nao coloco barra "/" mas sim ".." para cada diretorio que estou voltando a api se encoarrega de colocar a barra correta para o SO.
        filename: (req, file, cb) => {
            const ext = path.extname(file.originalname);
            const name = path.basename(file.originalname, ext);

            cb(null,`${name}-${Date.now()}${ext}`);
        },
    }),
};